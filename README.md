# docker-discourse

FORK OF:
https://lab.libreho.st/libre.sh/docker/discourse

Discourse image for discourse service
It is based on [upstream discourse image](https://hub.docker.com/r/discourse/base/)

## Discourse plugins

This image supports installing Discourse plugins at build time, via the `install/plugin-list` file. It lists`git` URLs of plugins to be installed at build time.
